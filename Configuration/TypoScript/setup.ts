## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextaddress {

        # Standard PID aus den Konstanten
        persistence {
            storagePid = {$plugin.tx_hiveextaddress.persistence.storagePid}
        }

        model {
            HIVE\HiveExtAddress\Domain\Model\Address {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Address.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\City {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\City.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\StateProvince {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\StateProvince.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Country {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Country.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Region {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Region.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Zip {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Zip.persistence.storagePid}
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Coordinate {
                persistence {
                    storagePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Coordinate.persistence.storagePid}
                }
            }
        }

        classes {
            HIVE\HiveExtAddress\Domain\Model\Address.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Address.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\City.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\City.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\StateProvince.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\StateProvince.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\Country.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Country.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\Region.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Region.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\Zip.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Zip.persistence.storagePid}
            HIVE\HiveExtAddress\Domain\Model\Coordinate.newRecordStoragePid = {$plugin.tx_hiveextaddress.model.HIVE\HiveExtAddress\Domain\Model\Coordinate.persistence.storagePid}
        }

    }
}