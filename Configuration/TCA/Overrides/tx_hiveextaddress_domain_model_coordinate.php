<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'lat';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'lon, title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY lat ASC, lon ASC';

$GLOBALS['TCA'][$sModel]['columns']['lat']['config'] = [
    'type' => 'input',
    'size' => 30,
    'eval' => 'required'
];
$GLOBALS['TCA'][$sModel]['columns']['lon']['config'] = [
    'type' => 'input',
    'size' => 30,
    'eval' => 'required'
];