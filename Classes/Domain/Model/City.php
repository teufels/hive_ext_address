<?php
namespace HIVE\HiveExtAddress\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * City
 */
class City extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * stateProvince
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\StateProvince
     */
    protected $stateProvince = null;

    /**
     * zip
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Zip>
     * @cascade remove
     */
    protected $zip = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the stateProvince
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\StateProvince $stateProvince
     */
    public function getStateProvince()
    {
        return $this->stateProvince;
    }

    /**
     * Sets the stateProvince
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\StateProvince $stateProvince
     * @return void
     */
    public function setStateProvince(\HIVE\HiveExtAddress\Domain\Model\StateProvince $stateProvince)
    {
        $this->stateProvince = $stateProvince;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->zip = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Zip
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Zip $zip
     * @return void
     */
    public function addZip(\HIVE\HiveExtAddress\Domain\Model\Zip $zip)
    {
        $this->zip->attach($zip);
    }

    /**
     * Removes a Zip
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Zip $zipToRemove The Zip to be removed
     * @return void
     */
    public function removeZip(\HIVE\HiveExtAddress\Domain\Model\Zip $zipToRemove)
    {
        $this->zip->detach($zipToRemove);
    }

    /**
     * Returns the zip
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Zip> $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Zip> $zip
     * @return void
     */
    public function setZip(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $zip)
    {
        $this->zip = $zip;
    }
}
